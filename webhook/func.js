const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')
const axios = require('axios');

fdk.handle(function(input){
  const hookID = input.hookID
  if (hookID == undefined) {
    return {"message": "missing hookID"}
  }

  const text = input.text
  if (text == undefined) {
    return {"eror": "missing text"}
  }

  const url = 'https://hooks.slack.com/services/' + input.hookID
  return axios.post(url, {
    "text": text
  })
  .then((res) => {
    return {"message": "success"}
  })
  .catch((err) => {
    return {"messsage":"slack error"}
  })
})